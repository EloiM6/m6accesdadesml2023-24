# Per escriure amb python
print("Hello")

# Per obtenir dades del teclat
'''
entrada = input("Entrada per teclat: ")
print(entrada)
'''

Num1 = 20
Num2 = 15.5
# Exemple de if
if Num1 > Num2:
    print("Num1 és més gran")
elif Num2 > Num1:
    print("Num2 és més gran")
else:
    print("Són iguals")

for i in range(11):
    print("El valor és " + str(i))
else:
    print("S'ha acabat")

entrada = "Hola, soy Lucas y no me gusta visual studio code ni el discord en negro"
#print inverteix l'array
print(entrada[::-1])
# Se salta els caràcters parells
print(entrada[::2])

# split crea un array amb la separació de l'espai en blanc de la cadena de text
llista = entrada.split()
print(llista)
for i in llista:
    print(i)

# Afegir un element
llista.append(".")
for i in llista:
    print(i)

# Borrar un element
del llista[len(llista) - 1 ]

for i in llista:
    print(i)
# Declarar una llista buida
llistabuida =[]

# Concatenar llistes
llista3 = [1,2,3]
llista4 = ['a','b']
llista5 = llista3 + llista4
print(llista5)

# Vincular llistes
llista4 = llista3
llista4[1] = 100000
print(llista3[1])

# Copiar llistes
llista4 = llista3[:]
llista4[1] = 5555
print(llista3)
print(llista4)

# Llista valors amb un index negatius
llista6=['Calzoncillos','Calcetines','Deberes','Pijamas']
print(llista6[-1])
print(llista6[-2])
print(llista6[-3])
print(llista6[-4])

# Vistes de les llistes
print(llista6[0:2])
print(llista6[1:3])

print(entrada[0:10])
print(entrada[15:30])

# Comprovar si un element es troba en una llista
print("pijamas" in llista6)
print("Pijamas" in llista6)

# Exemple de tupla -> És un enum en java
tupla1=(1,2,3,4,5,6,7,8,9)

# Exemple imprimir tupla
for i in tupla1:
    print(i, end=" ")
else:
    print("")#, end="\n")
    print("Final","Llista",sep="#")

# Exemple de diccionari
diccionari = {1:"Piedaá",2:"Roca",3:"Carbon"}
for i in diccionari:
    print(diccionari[i], end=" ")
else:
    print()
# Afegir un element
diccionari[4] = "PHP"

for i in diccionari:
    print(diccionari[i], end=" ")
else:
    print()