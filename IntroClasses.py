import mypackage.myclasses as classes

persona1= classes.Persona("Pepito","Studio")
persona2= classes.Persona("Bonito","Too")

print(persona1.Nomcomplet())
print(persona1.__dict__)
print(persona1.__dict__.keys())
#print(persona1.__varoculta)

print(classes.Persona.Counter)

Empleat1 = classes.Treballador("Ethan", "Corchero",20,50)
Empleat2 = classes.Treballador("Oscar", "Troya",22,100)

print(classes.Persona.Counter)
print(classes.Treballador.CounterTreballador)
print(Empleat1.Nomcomplet())
print(Empleat1 + Empleat2)
print(dir(Empleat1))